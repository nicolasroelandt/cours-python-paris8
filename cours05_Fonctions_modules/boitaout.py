#!/usr/bin/env python3

"""
Module Boitaout
Boite à outils contenant des fonctions utiles
"""

def contient(chaine, recherche):
    if(recherche in chaine):
        return True
    else:
        return False
    
# test
if __name__ == '__main__':
    contient("toto va à la plage", "toto")
    contient("toto va à la plage", "tata")