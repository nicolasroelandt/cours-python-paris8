# Programmation orientée objet avec Python 3 

- Université Paris8
-  Master 1 G2M (UFR Erites)
- Année 2019-2020

# Auteur 

[Nicolas Roelandt](https://roelandtn.frama.io/)

# Licence

Ce travail est sous licence [Creatives Commons - BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).

Destinés à l'enseignement vous pouvez réutiliser et modifier ces supports pour tout usage d'enseignement
à l'exception des usages commerciaux.

Vous devez néanmoins citer l'auteur originel.

# Inspirations

Ce cours a été fortement influencé par les [MOOC Python de l'Université de Côte-d'Azur](https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/about)
et les vidéos de [Corey Schafer](https://www.youtube.com/user/schafer5).
Le module Pandas est celui de [Daniel Chen](https://github.com/chendaniely/scipy-2019-pandas).