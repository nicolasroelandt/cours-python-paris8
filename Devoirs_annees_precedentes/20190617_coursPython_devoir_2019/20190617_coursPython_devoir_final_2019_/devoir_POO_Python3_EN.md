---
title: Devoir Programmation orienté objet avec python (English version)
author: 
    - Master 1 Géomatique, Géodécisionnel, Géomarketing et multimédia
    - 2018-2019 
date: 17 juin 2019
geometry: "left=1.5cm,right=1.5cm,top=1.5cm,bottom=1.5cm"
output: pdf_document
header-includes:
  - \usepackage{xcolor}
fontsize: 10pt

---

Duration: 3 hours

Please answer in only one jupter notebook or python script, that you will return with the outpu files.
# Lecture questions (4 pts)

1. What are the base types in Python ?
2. What is a *set* ? What is its most useful property ?
3. What is the use of the special method *\_\_init\_\_()* ? How is it called generally ?
4. What is inheritance ?

# Exercices simples (6 pts)
1. Create the  `batman` dictionary with the following properties :
  * nam : Batman
  * real identity : Bruce Wayne
  * status : Billionnaire
  * team: Justice League
  * ennemy: The Joker 

2. How to extract the information on the ennemy of Batman ?
3. Create a loop to display Batman's properties. 
4. Show three ways to display a variable in a string.
5. I have two lists : [\`pomme\`, \`poire\`, \`fraise\`, \`tomate\`] and [\`pomme de terre\`, \`poire\`, \`courgette\`, \`tomate\`]. 
How to get the elements common to the two lists ? How to get an ensmeble of thoses two list without duplication ? Provide your code. 

# Complexe exercice (10 pts)
This exercice aims to create train tickets from data contained in the `voyages.csv` file and save them into `billets.txt`.

## Data loading

1. Open `voyages.csv`
2. Loop through the lines of the file (except the first one)
3. For each line of the file, extract thoses informations:
  * Traveller name
  * Traveller first name
  * Traveller's fare
  * Travel date
  * Departure
  * Arrival
  * Distance between departure and arrival

4. Stock those information into `infoBillet` dictionnary with the following attributs :
(no accents or capitale letters)
  * nom
  * prenom
  * tarif
  * date
  * depart
  * arrivee
  * distance
5. Stock all`infoBillet` into a `billets` list as the loop progress.  

FOr this part, the use of *with open()* and *csv* module is recommended but not mandatory.


## Create the `Billet` class
### `Billet` class attributs
- nom
- prenom
- tarif
- date
- depart
- arrivee
- distance

### `Billet` class methods 

* Define the constructor that takes an `infoBillet` dictionnary as parameter.
* Create the method `prixBillet` which returns the ticket price with this calculation:
Normal price (*adulte*) is 1,5€ per kilometer (1,5 x distance). Séniors have got a 15% discount (normal price x 0.85) and childs a 30% discount (normal price x 0.70) 
* Create the method `printBillet` who return the ticket with that formatting (**Careful : multilign string**):
```
Traveller first name Traveller name
Tarif
Departure - Arrival
Date
Ticket price in euros
```


## Tickets saving
### Instantiation
1. For each ticket in `billets` list, instantiate a new object with the `Billet` class.
2. For each ticket in `billets` list, write the string provided by the
`printBillet` method into the file `billets.txt`. Each billet has to be seperated from the next on by a blank line,  30 \* and another blanck line.

Expected result:

```

        Jean Naymar 
        14/03/2019
        Compiègne - Paris
        126.0€
        
******************************

        Judas Bricot 
        25/04/2019
        Paris - Poitiers
        351.74999999999994€
        
******************************

        Cara Melmou 
        04/05/2019
        Les Aubrais - Martigues
        1125.0€
        
******************************

        Annie Versaire 
        04/05/2019
        Paris - Rennes
        561.0€
        
******************************
```

