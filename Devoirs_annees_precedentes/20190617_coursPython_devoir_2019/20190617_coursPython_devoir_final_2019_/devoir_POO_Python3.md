---
title: Devoir Programmation orienté objet avec python 
author: 
    - Master 1 Géomatique, Géodécisionnel, Géomarketing et multimédia
    - 2018-2019 
date: 17 juin 2019
geometry: "left=1.5cm,right=1.5cm,top=1.5cm,bottom=1.5cm"
output: pdf_document
header-includes:
  - \usepackage{xcolor}
fontsize: 10pt

---

Durée: 3 heures

Merci de répondre aux questions dans un seul document jupyter notebook ou script python (plus fichiers produits).

# Questions de cours (4 pts)

1. Quels sont les types de base en Python ?
2. Qu'est-ce qu'un set (ou *ensemble*) ? Quelle est sa propriété la plus utile ?
3. A quoi sert la méthode spéciale *\_\_init\_\_()* ? Comment l'appelle-t-on généralement ?
4. Qu'est-ce que l'héritage ?

# Exercices simples (6 pts)
1. Créez un dictionnaire `batman` ayant les propriétés suivantes:
  * nom : Batman
  * identité réelle: Bruce Wayne
  * statut : milliardaire
  * équipe: Ligue des Justiciers
  * ennemi: Le Joker 

2. Comment extraire l'information concernant l'ennemi de Batman ?
3. Créez une boucle affichant les propriétés de Batman 
4. Montrez trois méthodes pour inclure une variable dans une chaine de caractère
5. Je dispose des listes [\`pomme\`, \`poire\`, \`fraise\`, \`tomate\`] et [\`pomme de terre\`, \`poire\`, \`courgette\`, \`tomate\`]. Comment
obtenir les éléments communs aux deux listes ? Obtenir un ensemble des deux listes sans les doublons ? Fournissez votre code. 

# Exercice complexe (10 pts)
Cet exercice a pour objectif d'imprimer des billets de train à partir des voyages
contenus dans le fichier `voyages.csv` et d les stocker dans un fichier `billets.txt`

## Chargement des données

1. Ouvrir le fichier `voyages.csv`
2. Parcourir les lignes du fichier (sauf la première)
3. Pour chaque ligne du fichier, extraire les informations suivantes:
  * Nom du voyageur
  * Prénom du voyageur
  * Tarif du voyageur
  * Date du voyage
  * Ville de départ
  * Ville d'arrivée
  * Distance entre les deux villes
4. Stocker les informations extraites dans un dictionnaire `infoBillet` ayant les attributs suivants
(attention pas d'accents ni majuscule):
  * nom
  * prenom
  * tarif
  * date
  * depart
  * arrivee
  * distance
5. Stocker toutes les `infoBillet` dans une liste `billets` au fur et à mesure de la lecture des lignes
  
Pour cette question, l'utilisation de *with open()* et du module *csv* est recommandée mais n'est pas obligatoire.


## Créer une classe  `Billet`
### Attributs de la classe `Billet`
- nom
- prenom
- tarif
- date
- depart
- arrivee
- distance

### Méthodes de la classe `Billet` 

* définir le constructeur prenant en argument un dictionnaire `infoBillet`.
* Créer une méthode `prixBillet` qui retourne le prix du billet en fonction du tarif aucquel à droit le voyageur.
Le tarif normal (*adulte*) est d'1,5€ par kilomètre (1,5 x distance). Les Séniors bénéficient d'une
réduction de 15% (tarif normal x 0.85) et les enfants de 30% (tarif normal x 0.70) 
* Créer une méthode `printBillet` qui retourne le billet selon cette mise en forme (**attention chaine de caractères multiligne**):
```
prenom du Voyageur nom du Voyageur
Tarif
Départ - Arrivée
Date
Prix du billet en euros
```


## Sauvegarde des billets
### Instanciation
1. Pour chaque billet présent dans la liste `billets`, instancier un nouvel objet à l'aide
de la classe `Billet`.
2. Pour chaque billet présent dans la liste `billets`, écrire la chaine de caractère fournie par
`printBillet` dans un fichier `billets.txt`. Chaque billet doit être séparé du suivant par 
une ligne vide, une ligne de 30 \* et une ligne vide.

Résultat attendu:

```

        Jean Naymar 
        14/03/2019
        Compiègne - Paris
        126.0€
        
******************************

        Judas Bricot 
        25/04/2019
        Paris - Poitiers
        351.74999999999994€
        
******************************

        Cara Melmou 
        04/05/2019
        Les Aubrais - Martigues
        1125.0€
        
******************************

        Annie Versaire 
        04/05/2019
        Paris - Rennes
        561.0€
        
******************************
```

